import requests
import json
from rest_framework import viewsets, permissions
from rest_framework.response import Response
from .models import Pessoa
from .serializers import PessoaSerializer


class PessoaViewSetAdmin(viewsets.ModelViewSet):
    http_method_names = ['post', 'put', 'delete']
    queryset = Pessoa.objects.all()
    serializer_class = PessoaSerializer
    permission_classes = [permissions.IsAdminUser]

    def create(self, request, *args, **kwargs):
        serializer = PessoaSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        bearer = get_token_garagem_service()

        payload = json.dumps({
            "telefone": request.data['telefone'],
            "email": request.data['email']
        })
        headers = {
            'Authorization': f'Bearer {bearer}',
            'Content-Type': 'application/json'
        }
        requests.request("POST", 'http://webgaragem:8002/garagem/',
                         headers=headers, data=payload)

        return Response({}, status=201)


class PessoaViewSet(viewsets.ModelViewSet):
    http_method_names = ['get']
    queryset = Pessoa.objects.all()
    serializer_class = PessoaSerializer
    permission_classes = [permissions.IsAuthenticated]


def get_token_garagem_service():
    payload = json.dumps({
        "username": "teste",
        "password": "123"})

    response = requests.request(
        "POST", "http://webgaragem:8002/api/token/",
        headers={'Content-Type': 'application/json'},
        data=payload)

    return json.loads(response.text)['access']
