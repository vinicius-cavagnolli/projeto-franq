from django.db import models
from django.contrib.auth.models import AbstractUser


class Pessoa(AbstractUser):
    telefone = models.CharField(max_length=20)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    username = models.CharField(max_length=50, null=True)
    email = models.EmailField(unique=True)

    @property
    def nome(self):
        return self.first_name + self.last_name

    class Meta:
        ordering = ('first_name', 'last_name',)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ('telefone', 'first_name', 'last_name', 'password',)
