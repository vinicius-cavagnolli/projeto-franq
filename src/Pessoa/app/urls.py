from django.urls import include, path
from rest_framework import routers
from .views import PessoaViewSet, PessoaViewSetAdmin

router = routers.DefaultRouter()
router.register('pessoa', PessoaViewSet)

router_admin = routers.DefaultRouter()
router_admin.register('pessoa', PessoaViewSetAdmin)


urlpatterns = [
    path('admin/', include(router_admin.urls)),
    path('', include(router.urls))
]
