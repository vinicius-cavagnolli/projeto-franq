from rest_framework import serializers, viewsets
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from .serializers import AggGaragensSerializer, VeiculoCarroSerializer, VeiculoMotoSerializer, VeiculoSerializer
from .models import Consumidor, Garagem, Veiculo


class CriarConsumidorView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        consumidor = Consumidor(
            telefone=request.data['telefone'], email=request.data['email'])
        consumidor.save()

        garagem = Garagem(consumidor=consumidor)
        garagem.save()

        return Response(status=201)


class EscolherVeiculoView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        consumidor = Consumidor.objects.get(pk=request.data['consumidor'])
        garagem = Garagem.objects.get(consumidor=consumidor)
        veiculo = Veiculo.objects.get(pk=request.data['veiculo'])
        veiculo.garagem = garagem
        veiculo.save()

        return Response(status=200)


class DevolverVeiculoView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, id_veiculo):
        veiculo = Veiculo.objects.get(pk=id_veiculo)
        veiculo.garagem = None
        veiculo.save()

        return Response(status=200)


class VeiculosPorConsumidorView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id_consumidor):
        consumidor = Consumidor.objects.get(pk=id_consumidor)
        garagem = Garagem.objects.get(consumidor=consumidor)
        veiculos = Veiculo.objects.filter(garagem=garagem)

        serializer = VeiculoSerializer(veiculos, many=True)

        return Response(serializer.data)


class VeiculoView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id_veiculo):
        veiculo = Veiculo.objects.get(pk=id_veiculo)

        serializer = VeiculoCarroSerializer(veiculo) \
            if veiculo.tipo == 1 \
            else VeiculoMotoSerializer(veiculo)

        return Response(serializer.data)


class VeiculoViewSetAdmin(viewsets.ModelViewSet):
    queryset = Veiculo.objects.all()
    serializer_class = VeiculoSerializer
    permission_classes = [IsAdminUser]


class VeiculoDisponivelViewSet(viewsets.ModelViewSet):
    http_method_names = ['get']
    queryset = Veiculo.objects.filter(garagem=None)
    serializer_class = VeiculoSerializer
    permission_classes = [IsAuthenticated]


class AggDadosGaragensView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        consumidores = Consumidor.objects.all()

        data = []
        for consumidor in consumidores:
            garagem = Garagem.objects.get(consumidor=consumidor)
            veiculos = Veiculo.objects.filter(garagem=garagem)

            data.append({
                'consumidor': consumidor,
                'garagem': garagem,
                'veiculos': veiculos})

        serializer = AggGaragensSerializer(data, many=True)

        return Response(serializer.data)
