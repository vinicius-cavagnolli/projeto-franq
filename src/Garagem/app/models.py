from django.db import models

TIPO_VEICULO = ((1, 'Carro'), (2, 'Moto'))


class Consumidor(models.Model):
    telefone = models.CharField(max_length=20)
    email = models.EmailField(unique=True)


class Garagem(models.Model):
    consumidor = models.ForeignKey(Consumidor, on_delete=models.RESTRICT)


class Veiculo(models.Model):
    garagem = models.ForeignKey(
        Garagem, on_delete=models.RESTRICT, null=True)
    tipo = models.IntegerField(choices=TIPO_VEICULO)
    cor = models.CharField(max_length=30)
    ano = models.IntegerField()
    modelo = models.CharField(max_length=30)
