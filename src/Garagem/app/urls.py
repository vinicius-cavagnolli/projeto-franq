from django.urls import include, path
from rest_framework import routers
from .views import CriarConsumidorView, VeiculoDisponivelViewSet, VeiculoView, VeiculoViewSetAdmin, \
    EscolherVeiculoView, DevolverVeiculoView, VeiculosPorConsumidorView, AggDadosGaragensView

router = routers.DefaultRouter()
router.register('veiculos_disponiveis', VeiculoDisponivelViewSet)

router_admin = routers.DefaultRouter()
router_admin.register('veiculo', VeiculoViewSetAdmin)

urlpatterns = [
    path('garagem/', CriarConsumidorView.as_view()),
    path('agg_garagem/', AggDadosGaragensView.as_view()),
    path('escolher_veiculo/', EscolherVeiculoView.as_view()),
    path('devolver_veiculo/<int:id_veiculo>/', DevolverVeiculoView.as_view()),
    path('veiculos_consumidor/<int:id_consumidor>/', VeiculosPorConsumidorView.as_view()),
    path('veiculo/<int:id_veiculo>/', VeiculoView.as_view()),
    path('admin/', include(router_admin.urls)),
    path('', include(router.urls)),
]
