from rest_framework import serializers
from .models import Veiculo, Consumidor, Garagem


class VeiculoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Veiculo
        fields = '__all__'


class ConsumidorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Consumidor
        fields = '__all__'


class GaragemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Garagem
        fields = '__all__'


class VeiculoCarroSerializer(serializers.Serializer):
    cor = serializers.CharField()
    ano = serializers.IntegerField()


class VeiculoMotoSerializer(serializers.Serializer):
    modelo = serializers.CharField()
    ano = serializers.IntegerField()


class AggGaragensSerializer(serializers.Serializer):
    consumidor = ConsumidorSerializer()
    garagem = GaragemSerializer()
    veiculos = VeiculoSerializer(many=True)
